'use strict';

import React from 'react/addons';

import { Link } from 'react-router';

const Post = React.createClass({

    propTypes: {
        user: React.PropTypes.object,
        post: React.PropTypes.object
    },

    render() {
        let user = this.props.user;
        let post = this.props.post;

        let comment_views = [];
        for (var i=0; i < post.actions.length; i++) {
          comment_views.push(
            <div className="col-md-2 commented">
                <div>
                    <figure className="avatar">
                        <img src={post.actions[i].photo } />
                    </figure>
                </div>
            </div>
          );
        }

        return (
          <div className="row">
              <div className="col-md-8 col-md-offset-2">
                  <div className="questions-main-container">
                      <div className="row">
                          <div className="col-md-9">
                              <div className="single-question-container">
                                  <figure className="avatar">
                                      <img src={ post.photo }/>
                                  </figure>
                                  <div className="question">
                                      <span className="username">{ post.username }</span> is asking:
                                      <p className="question-text">{ post.question }</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-md-9">
                              <div className="activity">
                                  <div className="row">
                                      <div className="col-md-2 asked">
                                          <div>asked</div>
                                      </div>
                                      <div className="col-md-2 more-activities"><div>{ post.more_activities }</div></div>

                                      { comment_views }
                                  </div>
                              </div>
                          </div>
                          <div className="col-md-3">
                              <ul className="statistics">
                                  <li><strong>{ post.discussions }</strong> related discussion</li>
                                  <li><strong>{ post.peers }</strong> peers involved</li>
                                  <li><strong>{ post.conversations }</strong> conversations</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        );
    }
});

export default Post;
