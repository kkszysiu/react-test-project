'use strict';

const posts = [
  {
    username: 'Eddie',
    photo: 'http://images.moviefanatic.com/iu/s--GQlBmnbD--/t_thumb/f_auto,fl_lossy,q_75/v1364991209/eddie-murphy-wtf.jpg',
    question: 'Would you rather go into the future or the past?',
    actions: [
      {username: 'Guy A', type: 'comment', photo: 'https://media.licdn.com/mpr/mpr/shrink_100_100/AAEAAQAAAAAAAAHEAAAAJGZiMzY4NTU1LTQ3N2ItNGYzNS1hN2MxLWMxMDU0ZTIxMjU2Mg.jpg'},
      {username: 'Chick B', type: 'comment', photo: 'https://yt3.ggpht.com/-5v4woDfwZWQ/AAAAAAAAAAI/AAAAAAAAAAA/SLeL3axOxO8/s100-c-k-no/photo.jpg'},
      {username: 'Guy C', type: 'comment', photo: 'http://asianwiki.com/images/7/77/The_Metropolitan_Police_Missing_Person-Masaya_Kikawada.jpg'},
      {username: 'Chick D', type: 'comment', photo: 'http://usercontent1.hubimg.com/12363776_100.jpg'}
    ],
    conversations: 7,
    discussions: 2,
    peers: 5,
    more_activities: 5
  }
];

const constants = {
    appName: 'SecretAsFuckProject',
    posts: posts
};

export default constants;
