'use strict';

import React from 'react/addons';
import Reflux from 'reflux';

import { Router, Route, Redirect } from 'react-router';
import { history } from 'react-router/lib/HashHistory';

import constants from './util/constants';

import Actions from './actions/Actions';

import Homepage from './views/Homepage';
import UhOh from './views/404';

import injectTapEventPlugin from 'react-tap-event-plugin';

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

document.title = constants.appName;

let App = React.createClass({

    propTypes: {
        children: React.PropTypes.object
    },

    contextTypes: {
      router: React.PropTypes.func
    },


    getInitialState() {
        return {
            questions: true,
            searchString: '',
            search: ''
        };
    },

    handleSortClick(type) {
      console.log(type);
    },

    handleSearchClick(ev) {
      this.setState({
        search: this.state.searchString
      });
      Actions.searchPost(this.state.searchString);

      ev.preventDefault();
      return
    },

    handleSearchInputChange(event) {
      console.log(event.target.value);
      this.setState({
        searchString: event.target.value
      });
    },

    handleQuestionsChange(event) {
      this.setState({
        questions: !this.state.questions
      });
    },

    render() {
        let modal = this.state.modal;
        let result = {};

        return (
          <div className="wrapper">

          <header>
              <div className="container">
                  <div className="row">
                      <div className="col-md-2 col-sm-2 col-xs-2">
                          <a href="#" className="arrow"><i className="icon-left-open-big"></i></a>
                      </div>
                      <div className="col-md-6">
                          <a href="#" className="questions">Questions <i className="icon-plus-circle"></i></a>
                          <div className="radio-container">
                              <label>
                                  <input type="radio" name="sort" checked={this.state.questions === true} onChange={this.handleQuestionsChange} />
                                  My shelf
                              </label>
                              <label>
                                  <input type="radio" name="sort" checked={this.state.questions === false} onChange={this.handleQuestionsChange} />
                                  All questions
                              </label>
                          </div>
                      </div>
                      <div className="col-md-2 text-right">
                          <div className="sort-by"><span>Sort by:</span>
                              <a href="#" onClick={this.handleSortClick.bind(this, 'recent')} className="recent">recent</a>
                              <span> or</span> <a href="#" onClick={this.handleSortClick.bind(this, 'hot')} className="hot">hot</a>
                          </div>
                      </div>
                  </div>
                  <div className="row search-container">
                      <form>
                          <div className="col-md-6 col-md-offset-2">
                              <input type="search" name="search" className="form-control" placeholder="Search questions" value={this.state.searchString} onChange={this.handleSearchInputChange} />
                          </div>
                          <div className="col-md-2">
                              <button type="submit" className="btn btn-default btn-block btn-search" onClick={this.handleSearchClick}>Search</button>
                          </div>
                      </form>
                  </div>
              </div>
          </header>

          <div className="content">
            { this.props.children || <Homepage/> }
          </div>

          <footer></footer>
          </div>
        );
    }
});

React.render((
    <Router history={ history }>
        <Route component={ App }>
            <Route name="home" path="/" component={ Homepage } />
            <Route name="404" path="/404" component={ UhOh } />
            {/* Redirects */}
            <Redirect from="*" to="/404" />
        </Route>
    </Router>
), document.getElementById('app'));
