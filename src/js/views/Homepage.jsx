'use strict';

import React from 'react/addons';
import Reflux from 'reflux';
import Actions from '../actions/Actions';
import { Navigation, TransitionHook } from 'react-router';

import PostsStore from '../stores/PostsStore';

import constants from '../util/constants';

import Post from '../components/Post';
import Link from 'react-router/lib/Link';

import _ from 'lodash';

const Homepage = React.createClass({

    propTypes: {
        params: React.PropTypes.object
    },

    mixins: [
        TransitionHook,
        Navigation,
        Reflux.listenTo(Actions.searchPost, 'submitPostSearchCompleted')
    ],

    getInitialState() {
      var postsData = {};
        return {
            posts: constants.posts,
            loading: true,
            sortOptions: postsData.sortOptions,
            nextPage: postsData.nextPage,
            currentPage: postsData.currentPage
        };
    },

    submitPostSearchCompleted(searchString) {
      console.log('submitPostSearchCompleted', searchString);
      var posts = constants.posts;
      var new_posts = [];

      for (var i=0; i < posts.length; i++) {
        if (_.contains(posts[i].question.toLowerCase(), searchString.toLowerCase())) {
          new_posts.push(posts[i]);
        }
      }

      this.setState({
        posts: new_posts
      });
    },

    routerWillLeave() {
        //Actions.stopWatchingPosts();
    },

    render() {
      console.log(this.state.posts);
      let post_views = [];
      for (var i=0; i < this.state.posts.length; i++) {
        post_views.push(<Post key={ i } post={ this.state.posts[i] } />);
      }
      return (
        <div className="container">
          { post_views }
        </div>
      );
    }

});

export default Homepage;
