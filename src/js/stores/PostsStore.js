'use strict';

import Reflux from 'reflux';
import Actions from '../actions/Actions';

const PostsStore = Reflux.createStore({

    listenables: Actions,

    getDefaultData() {
        return {};
    }

});

export default PostsStore;
