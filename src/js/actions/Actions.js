'use strict';

import Reflux from 'reflux';

const Actions = Reflux.createActions({
    'searchPost': {}
});

/* User Actions
=============================== */

Actions.searchPost.listen(function(searchString) {
  return searchString;
});

export default Actions;
